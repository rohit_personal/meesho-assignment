package com.meesho.assignment.di

import androidx.room.Room
import com.google.gson.GsonBuilder
import com.meesho.assignment.model.AppDatabase
import com.meesho.assignment.model.repository.MeeshoRepository
import com.meesho.assignment.network.HeaderInterceptor
import com.meesho.assignment.network.NetworkService
import com.meesho.assignment.ui.pull_requests.PullRequestsViewModel
import com.meesho.assignment.ui.repo.RepoViewModel
import com.meesho.assignment.ui.splash.SplashViewModel
import com.meesho.assignment.utils.MConstants
import com.meesho.assignment.utils.MPrefs
import com.meesho.assignment.utils.SharedPreferencesHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {

    single { SharedPreferencesHelper(get()) }
    single { MPrefs(get()) }
    single { MeeshoRepository(get(), get(), get()) }

    single { GsonBuilder().create() }
    single { GsonConverterFactory.create(get()) }
    single { RxJava2CallAdapterFactory.create() }
    single { HeaderInterceptor() }
    single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC } }
    single {
        OkHttpClient().newBuilder().apply {
            addInterceptor(get<HeaderInterceptor>())
            addInterceptor(get<HttpLoggingInterceptor>())
            connectTimeout(0, TimeUnit.SECONDS)
            readTimeout(0, TimeUnit.SECONDS)
            writeTimeout(0, TimeUnit.SECONDS)
        }.build()
    }
    single {
        Retrofit.Builder().apply {
            client(get())
            baseUrl(MConstants.BASE_URL)
            addConverterFactory(get<GsonConverterFactory>())
            addCallAdapterFactory(get<RxJava2CallAdapterFactory>())
        }.build()
    }
    single { get<Retrofit>().create(NetworkService::class.java) }

    single { Room.databaseBuilder(get(), AppDatabase::class.java, "github_db").build() }

    viewModel { SplashViewModel(get()) }
    viewModel { RepoViewModel(get()) }
    viewModel { PullRequestsViewModel(get()) }
}