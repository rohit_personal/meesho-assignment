package com.meesho.assignment.ui.webview


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.meesho.assignment.custom.LoadingDialog
import com.meesho.assignment.databinding.FragmentWebViewBinding
import com.meesho.assignment.utils.doOnProgressCompleted
import kotlinx.android.synthetic.main.fragment_web_view.*


class WebViewFragment : Fragment() {

    private val loadingDialog by lazy {
        LoadingDialog(requireActivity()).apply {
            setCancelable(false)
            lifecycle.addObserver(this)
        }
    }

    private lateinit var dataBinding: FragmentWebViewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dataBinding = FragmentWebViewBinding.inflate(inflater, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            dataBinding.url = WebViewFragmentArgs.fromBundle(it).url
        }

        loadingDialog.toggle(true)
        webView.doOnProgressCompleted { loadingDialog.toggle(false) }
    }
}
