package com.meesho.assignment.ui.repo

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.meesho.assignment.R
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.model.RepoDetails
import com.meesho.assignment.model.Repository
import com.meesho.assignment.model.Response
import com.meesho.assignment.model.repository.MeeshoRepository

class RepoViewModel(val repository: MeeshoRepository) : BaseViewModel() {

    val repoDetails = RepoDetails()

    val ownerNameErrorLiveData = MutableLiveData<Int>()
    val repoNameErrorLiveData = MutableLiveData<Int>()
    val responseLiveData = MutableLiveData<Response<Repository>>()

    override fun getBindingId(): Int = BR.viewModel

    fun getPullRequests() {
        when {
            repoDetails.ownerName.isEmpty() -> ownerNameErrorLiveData.postValue(R.string.repo_owner_error)
            repoDetails.repoName.isEmpty() -> repoNameErrorLiveData.postValue(R.string.repo_name_error)
            else -> getCompositeDisposable().add(repository.getRepo(
                repoDetails.ownerName, repoDetails.repoName
            ).doOnSubscribe { responseLiveData.postValue(Response.Loading(true)) }.subscribe({
                responseLiveData.postValue(Response.Success(it))
            }, { throwable ->
                responseLiveData.postValue(Response.Error(Exception(throwable)))
            })
            )
        }
    }
}
