package com.meesho.assignment.ui.repo

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.meesho.assignment.R
import com.meesho.assignment.base.BaseFragment
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.custom.LoadingDialog
import com.meesho.assignment.databinding.FragmentRepoBinding
import com.meesho.assignment.model.Response
import com.meesho.assignment.ui.NavigationActivity
import com.meesho.assignment.utils.getErrorMessage
import com.meesho.assignment.utils.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException

class RepoFragment : BaseFragment<FragmentRepoBinding>() {

    companion object {
        fun newInstance() = RepoFragment()
    }

    private val viewModel by viewModel<RepoViewModel>()
    private val loadingDialog by lazy {
        LoadingDialog(requireActivity()).apply {
            lifecycle.addObserver(this)
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_repo

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (requireActivity() as NavigationActivity).supportActionBar?.hide()

        viewModel.responseLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Response.Loading -> loadingDialog.toggle(response.status)
                is Response.Error -> {
                    if ((response.exception.cause as? HttpException)?.code() == 404) {
                        getString(R.string.repo_found_error).toast(requireContext())
                    } else {
                        response.exception.getErrorMessage(requireContext())?.toast(requireContext())
                    }
                    loadingDialog.toggle(false)
                }
                is Response.Success -> {
                    findNavController().navigate(R.id.action_repoFragment_to_pullRequestsFragment)
                    loadingDialog.toggle(false)
                }
            }
        })
    }

}
