package com.meesho.assignment.ui.splash


import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.meesho.assignment.R
import com.meesho.assignment.base.BaseFragment
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.databinding.FragmentSplashBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    companion object {
        fun newInstance() = SplashFragment()
    }

    private val splashViewModel by viewModel<SplashViewModel>()

    override fun getLayoutId(): Int = R.layout.fragment_splash

    override fun getViewModel(): BaseViewModel = splashViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        splashViewModel.repoSelectedLiveData.observe(viewLifecycleOwner, Observer { status ->
            if (!status)
                findNavController().navigate(R.id.action_splashFragment_to_repoFragment)
            else
                findNavController().navigate(R.id.action_splashFragment_to_pullRequestsFragment)
        })
    }
}
