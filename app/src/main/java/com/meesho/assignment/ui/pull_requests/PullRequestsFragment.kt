package com.meesho.assignment.ui.pull_requests


import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.content.getSystemService
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.meesho.assignment.R
import com.meesho.assignment.base.BaseFragment
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.custom.LoadingDialog
import com.meesho.assignment.databinding.FragmentPullRequestsBinding
import com.meesho.assignment.model.PullRequest
import com.meesho.assignment.model.Response
import com.meesho.assignment.ui.NavigationActivity
import com.meesho.assignment.utils.*
import kotlinx.android.synthetic.main.fragment_pull_requests.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class PullRequestsFragment : BaseFragment<FragmentPullRequestsBinding>(),
    PullRequestsAdapter.OnPullRequestsClickedListener {

    private val viewModel by viewModel<PullRequestsViewModel>()
    private val pullRequestsAdapter by lazy { PullRequestsAdapter(this) }
    private var isNetworkAvailable = false
    private val loadingDialog by lazy {
        LoadingDialog(requireActivity()).apply {
            lifecycle.addObserver(this)
        }
    }
    private val connectivityChecker by lazy {
        ConnectivityChecker(requireContext().getSystemService<ConnectivityManager>()!!).apply {
            lifecycle.addObserver(this)
        }
    }

    private val endlessRecyclerOnScrollListener by lazy {
        object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore(current_page: Int) {
                if (isNetworkAvailable) {
                    viewModel.getPullRequests(current_page)
                }
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_pull_requests

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getDataBinding().pullRequestsAdapter = pullRequestsAdapter

        viewModel.clearPullRequestsLiveData.observe(this, Observer { status ->
            if (status) {
                pullRequestsAdapter.clearPullRequests()
                endlessRecyclerOnScrollListener.reset()
            }
        })

        viewModel.pullRequestsLiveData.observe(this, Observer { response ->

            when (response) {
                is Response.Loading -> {
                    if (!pullRequestsRefresh.isRefreshing) {
                        loadingDialog.toggle(response.status)
                    }
                }
                is Response.Success -> {
                    pullRequestsAdapter.addPullRequests(response.data)
                    pullRequestsRefresh.isRefreshing = false
                    loadingDialog.toggle(false)
                }

                is Response.Error -> {
                    response.exception.getErrorMessage(requireContext())?.toast(requireContext())
                    pullRequestsRefresh.isRefreshing = false
                    loadingDialog.toggle(false)
                }

            }


        })

        viewModel.changeRepositoryLiveData.observe(this, SingleEventObserver { response ->

            when (response) {
                is Response.Loading -> {
                    loadingDialog.toggle(response.status)
                }
                is Response.Success -> {
                    loadingDialog.toggle(false)
                    if (response.data) {
                        findNavController().navigate(R.id.action_pullRequestsFragment_to_repoFragment)
                    }
                }

                is Response.Error -> {
                    response.exception.getErrorMessage(requireContext())?.toast(requireContext())
                    loadingDialog.toggle(false)
                }

            }


        })

        viewModel.repositoryNameLiveData.observe(viewLifecycleOwner, Observer { repoName ->
            (requireActivity() as NavigationActivity).supportActionBar?.apply {
                show()
                title = repoName
            }
        })

        connectivityChecker.connectedStatus.observe(viewLifecycleOwner, Observer { networkStatus ->
            isNetworkAvailable = networkStatus
        })

        pullRequestsRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener)

        setHasOptionsMenu(true)
    }

    override fun onPullRequestsClicked(pullRequest: PullRequest) {
        if (findNavController().currentDestination?.id == R.id.pullRequestsFragment) {
            findNavController().navigate(
                PullRequestsFragmentDirections.actionPullRequestsFragmentToWebViewFragment(
                    pullRequest.html_url
                )
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_pull_requests, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionChangeRepository)
            viewModel.changeRepository()
        return super.onOptionsItemSelected(item)
    }
}
