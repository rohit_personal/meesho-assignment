package com.meesho.assignment.ui.pull_requests

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.model.PullRequest
import com.meesho.assignment.model.Response
import com.meesho.assignment.model.repository.MeeshoRepository
import com.meesho.assignment.utils.SingleEvent

class PullRequestsViewModel(private val repository: MeeshoRepository) : BaseViewModel() {

    val repositoryNameLiveData = MutableLiveData<String>()
    val pullRequestsLiveData = MutableLiveData<Response<List<PullRequest>>>()
    val clearPullRequestsLiveData = MutableLiveData<Boolean>()
    val changeRepositoryLiveData = MutableLiveData<SingleEvent<Response<Boolean>>>()
    val FIRST_PAGE = 1

    override fun getBindingId(): Int = BR.viewModel

    init {
        getRepositoryName()
        getPullRequests(FIRST_PAGE)
    }

    fun getPullRequests(page: Int) {
        getCompositeDisposable().add(
            repository.getPullRequests(page).doOnSubscribe {
                if (page == 1) {
                    pullRequestsLiveData.postValue(
                        Response.Loading(true)
                    )
                }
            }.subscribe(
                { pullRequests ->
                    pullRequests?.let {
                        if (it.isNotEmpty()) {
                            clearPullRequestsLiveData.postValue(page == FIRST_PAGE)
                            pullRequestsLiveData.postValue(Response.Success(it))
                        }
                    }
                },
                { throwable ->
                    pullRequestsLiveData.postValue(Response.Error(Exception(throwable)))
                })
        )
    }

    private fun getRepositoryName() {
        getCompositeDisposable().add(repository.getRepositoryName().subscribe { repoName ->
            repositoryNameLiveData.postValue(
                repoName
            )
        })
    }

    fun changeRepository() {
        getCompositeDisposable().add(repository.changeRepository().doOnSubscribe {
            changeRepositoryLiveData.postValue(
                SingleEvent(Response.Loading(true))
            )
        }.subscribe({ success -> changeRepositoryLiveData.postValue(SingleEvent(Response.Success(success))) },
            { throwable ->
                changeRepositoryLiveData.postValue(SingleEvent(Response.Error(Exception(throwable))))
            })
        )
    }
}