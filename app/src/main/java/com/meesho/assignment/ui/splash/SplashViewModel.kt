package com.meesho.assignment.ui.splash

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.meesho.assignment.base.BaseViewModel
import com.meesho.assignment.model.repository.MeeshoRepository

class SplashViewModel(private val repository: MeeshoRepository) : BaseViewModel() {

    val repoSelectedLiveData = MutableLiveData<Boolean>()


    override fun getBindingId(): Int = BR.viewModel

    init {
        getRepoSelectedStatus()
    }

    private fun getRepoSelectedStatus() {
        getCompositeDisposable().add(repository.getRepoSelectedStatus().subscribe({ status ->
            repoSelectedLiveData.postValue(status)
        }, {
            repoSelectedLiveData.postValue(false)
        }))
    }
}