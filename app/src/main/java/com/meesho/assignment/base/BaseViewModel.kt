package com.meesho.assignment.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    // Add all RxJava subscriptions. Disposed when ViewModel is cleared
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    abstract fun getBindingId(): Int

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }

    fun getCompositeDisposable() = compositeDisposable
}