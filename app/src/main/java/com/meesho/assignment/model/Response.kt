package com.meesho.assignment.model

import com.meesho.assignment.model.Response.Success

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Response<out R> {

    data class Success<out T>(val data: T) : Response<T>()
    data class Error(val exception: Exception) : Response<Nothing>()
    data class Loading(val status: Boolean) : Response<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            is Loading -> "Loading[status=$status]"
        }
    }
}

/**
 * `true` if [Response] is of type [Success] & holds non-null [Success.data].
 */
val Response<*>.succeeded
    get() = this is Success && data != null
