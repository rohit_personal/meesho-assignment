package com.meesho.assignment.model.repository

import com.meesho.assignment.model.AppDatabase
import com.meesho.assignment.network.NetworkService
import com.meesho.assignment.utils.MPrefs
import com.meesho.assignment.utils.applySchedulers
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class MeeshoRepository(
    private val mPrefs: MPrefs,
    private val networkService: NetworkService,
    private val appDatabase: AppDatabase
) {

    // Items per page for query
    private val ITEMS_PER_PAGE = 20
    // Either open, closed, or all to filter by state.
    private val REQUEST_STATE = "all"

    fun getRepoSelectedStatus(): Single<Boolean> =
        Single.just(mPrefs.getRepoSelectedStatus()).delay(2, TimeUnit.SECONDS).applySchedulers()

    fun getRepo(ownerName: String, repositoryName: String) =
        networkService.getRepo(ownerName, repositoryName).map { repository ->
            if (repository.private) throw RuntimeException("This repository is not public")
            repository.ownerName = ownerName
            appDatabase.repositoryDao().apply {
                nukeTable()
                insert(repository)
            }
            mPrefs.setRepoSelected(true)
            mPrefs.setRepoName("$ownerName/$repositoryName")
            return@map repository
        }.applySchedulers()

    fun getPullRequests(page: Int) =
        getRepositoryFromDB().flatMap { repository ->
            networkService.getPullRequests(
                repository.ownerName, repository.name,
                page, ITEMS_PER_PAGE, REQUEST_STATE
            )
        }.map { pullRequests ->
            if (page == 1) {
                appDatabase.pullRequestDao().apply {
                    nukeTable()
                    insert(pullRequests)
                }
            }
            return@map pullRequests
        }.onErrorResumeNext { throwable ->
            if (page == 1)
                getPullRequestsFromDB()
            else
                Single.error(throwable)
        }.applySchedulers()


    fun getRepositoryName() = Single.just(mPrefs.getRepoName()).applySchedulers()

    fun changeRepository() = Single.fromCallable {
        appDatabase.pullRequestDao().nukeTable()
        appDatabase.repositoryDao().nukeTable()
        mPrefs.setRepoSelected(false)
        return@fromCallable true
    }.applySchedulers()

    private fun getRepositoryFromDB() = appDatabase.repositoryDao().getRepository()
    private fun getPullRequestsFromDB() =
        appDatabase.pullRequestDao().getPullRequests().map { pullRequests -> pullRequests.reversed() }.delay(
            1,
            TimeUnit.SECONDS
        ).applySchedulers()

}