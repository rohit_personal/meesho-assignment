package com.meesho.assignment.utils

/**
 * A helper class to hold all app preferences and states
 */

class MPrefs(private val sharedPreferencesHelper: SharedPreferencesHelper) {

    fun getRepoSelectedStatus() = sharedPreferencesHelper.getBoolean(MKeys.IS_REPO_SELECTED, false)

    fun setRepoSelected(status: Boolean) = sharedPreferencesHelper.putBoolean(MKeys.IS_REPO_SELECTED, status)

    fun getRepoName() = sharedPreferencesHelper.getString(MKeys.REPO_NAME, "")

    fun setRepoName(repoName: String) = sharedPreferencesHelper.putString(MKeys.REPO_NAME, repoName)
}