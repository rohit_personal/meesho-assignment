package com.meesho.assignment.utils

import android.app.Application
import com.meesho.assignment.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MeeshoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MeeshoApplication)
            modules(appModule)
        }
    }
}