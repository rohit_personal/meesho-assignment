package com.meesho.assignment.utils

object MKeys {
    const val IS_REPO_SELECTED = "is_repo_selected"
    const val REPO_NAME = "repo_name"
}