package com.meesho.assignment.network

import com.meesho.assignment.model.PullRequest
import com.meesho.assignment.model.Repository
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkService {

    @GET("{owner_name}/{repo_name}/pulls")
    fun getPullRequests(
        @Path("owner_name") ownerName: String,
        @Path("repo_name") repositoryName: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
        @Query("state") state: String
    ): Single<List<PullRequest>>

    @GET("{owner_name}/{repo_name}")
    fun getRepo(
        @Path("owner_name") ownerName: String,
        @Path("repo_name") repositoryName: String
    ): Single<Repository>
}