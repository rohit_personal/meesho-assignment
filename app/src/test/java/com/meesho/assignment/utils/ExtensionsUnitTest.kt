package com.meesho.assignment.utils

import org.junit.Assert
import org.junit.Test


class ExtensionsUnitTest {

    @Test
    fun `validate date formatting`() {
        Assert.assertEquals("2019-03-21T21:43:01Z".getFormattedDate(), "21 Mar")
        Assert.assertEquals("2019-02-18T02:52:58Z".getFormattedDate(), "18 Feb")
        Assert.assertEquals("2019-03-21".getFormattedDate(), "")
    }
}